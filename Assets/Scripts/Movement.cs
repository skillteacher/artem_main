using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 5.5f;
    [SerializeField] private float jumpForce = 10f;
    private Rigidbody2D playerRigidbody;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private string groundLayer = "Ground";
    [SerializeField] private CircleCollider2D feetCollider;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrounded;
    
    

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        SwitchAnimation(playerInput);
        Flip(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if(Input.GetKeyDown(jumpButton) && isGrounded)
        {
            Jump();
        }
    }

    private void Move(float direction)
    {
        playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity = jumpVector;
    }

    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Run", playerInput != 0);
    }

    private void Flip(float playerInput)
    {
        bool isSpriteFlip = playerSpriteRenderer.flipX;
        if(playerInput < 0)
        {
            playerSpriteRenderer.flipX = true;
        }
        if (playerInput > 0)
        {
            playerSpriteRenderer.flipX = false;
        }

    }
}
